<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\Tests\File;

use FileManagementTools\Encoding\Encoding;
use FileManagementTools\File\File;
use FileManagementTools\File\Path;
use FileManagementTools\Stream\StreamFactory;
use FileManagementTools\Stream\StreamInterface;
use FileManagementTools\TypeSniffer\FileType;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \FileManagementTools\File\File
 */
final class FileTest extends TestCase
{
    /**
     * @var string the file that will be tested
     */
    private $file;

    /**
     * @var string base path of test files
     */
    private $files;

    protected function setUp()
    {
        $this->files = Path::join(__DIR__, '../TypeSniffer/Files');
        $this->file  = tempnam(sys_get_temp_dir(), 'test');

        file_put_contents($this->file, "file\r\nwith\nseveral\nlines\nand\naccents\n\xe9 \xe0 \xe7");
    }

    protected function tearDown()
    {
        if (is_file($this->file)) {
            unlink($this->file);
        }
    }

    public function testExists(): void
    {
        static::assertTrue(File::exists($this->file));

        unlink($this->file);

        static::assertFalse(File::exists($this->file));
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testOpenRead(): void
    {
        File::openRead($this->file)->use(static function (StreamInterface $s): void {
            static::assertTrue($s->isReadable());
            static::assertFalse($s->isWritable());
            static::assertSame("file\r\nwith\nseveral\nlines\nand\naccents\n\xe9 \xe0 \xe7", $s->readAllBytes());
        })
        ;
    }

    /**
     * @expectedException \FileManagementTools\File\Exceptions\FileNotFoundException
     *
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testOpenReadWithInvalidFile(): void
    {
        File::openRead('some non existing file');
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testOpenWrite(): void
    {
        File::openWrite($this->file)->use(static function (StreamInterface $s): void {
            static::assertFalse($s->isReadable());
            static::assertTrue($s->isWritable());
            static::assertSame(6, $s->writeBytes('123456'));
        })
        ;
    }

    /**
     * @expectedException \FileManagementTools\File\Exceptions\FileNotFoundException
     *
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testOpenWriteWithInvalidFile(): void
    {
        File::openWrite('some non existing file', false);
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testOpenReadWrite(): void
    {
        File::openReadWrite($this->file)->use(static function (StreamInterface $s): void {
            static::assertTrue($s->isReadable());
            static::assertTrue($s->isWritable());
            static::assertSame(6, $s->writeBytes('123456'));
        })
        ;
    }

    /**
     * @expectedException \FileManagementTools\File\Exceptions\FileNotFoundException
     *
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testOpenReadWriteWithInvalidFile(): void
    {
        File::openReadWrite('some non existing file', false);
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testCopyTo(): void
    {
        StreamFactory::createEmptyStream()->use(function (StreamInterface $s): void {

            File::copyTo($this->file, $s);

            static::assertSame("file\r\nwith\nseveral\nlines\nand\naccents\n\xe9 \xe0 \xe7", $s->readAllBytes());
        })
        ;
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testReadBytes(): void
    {
        static::assertSame('several', File::readBytes($this->file, 11, 7, true));
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testReadAllBytes(): void
    {
        static::assertSame(
            "file\r\nwith\nseveral\nlines\nand\naccents\n\xe9 \xe0 \xe7",
            File::readAllBytes($this->file)
        );
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testReadAllText(): void
    {
        static::assertSame(
            "file\r\nwith\nseveral\nlines\nand\naccents\né à ç",
            File::readAllText($this->file, Encoding::WINDOWS_1252)
        );
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testReadAllTextWrongEncoding(): void
    {
        static::assertSame(
            "file\r\nwith\nseveral\nlines\nand\naccents\n\xe9 \xe0 \xe7",
            File::readAllText($this->file)
        );
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testLines(): void
    {
        static::assertSame(
            ['file', 'with', 'several', 'lines', 'and', 'accents', 'é à ç'],
            iterator_to_array(File::lines($this->file, Encoding::WINDOWS_1252))
        );
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testLinesWrongEncoding(): void
    {
        static::assertSame(
            ['file', 'with', 'several', 'lines', 'and', 'accents', "\xe9 \xe0 \xe7"],
            iterator_to_array(File::lines($this->file))
        );
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testWriteAllBytes(): void
    {
        File::writeAllBytes($this->file, "\xe9 \xe0 \xe7");

        static::assertSame("\xe9 \xe0 \xe7", File::readAllBytes($this->file));
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testAppendBytes(): void
    {
        File::appendBytes($this->file, "\xe9 \xe0 \xe7");

        static::assertSame(
            "file\r\nwith\nseveral\nlines\nand\naccents\n\xe9 \xe0 \xe7\xe9 \xe0 \xe7",
            File::readAllBytes($this->file)
        );
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testWriteAllText(): void
    {
        File::writeAllText($this->file, 'some text é à ç', false, Encoding::WINDOWS_1252);

        static::assertSame('some text é à ç', File::readAllText($this->file, Encoding::WINDOWS_1252));
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testAppendText(): void
    {
        File::appendText($this->file, ' some text é à ç', false, Encoding::WINDOWS_1252);

        static::assertSame(
            "file\r\nwith\nseveral\nlines\nand\naccents\né à ç some text é à ç",
            File::readAllText($this->file, Encoding::WINDOWS_1252)
        );
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testAppendTextWrongEncoding(): void
    {
        File::appendText($this->file, ' some text é à ç', false, Encoding::WINDOWS_1252);

        static::assertSame(
            "file\r\nwith\nseveral\nlines\nand\naccents\n\xe9 \xe0 \xe7 some text \xe9 \xe0 \xe7",
            File::readAllText($this->file)
        );
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testReading(): void
    {
        File::reading($this->file, static function (StreamInterface $s): void {
            static::assertTrue($s->isReadable());
            static::assertFalse($s->isWritable());
            static::assertSame("file\r\nwith\nseveral\nlines\nand\naccents\n\xe9 \xe0 \xe7", $s->readAllBytes());
        });
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testWriting(): void
    {
        File::writing($this->file, static function (StreamInterface $s): void {
            static::assertFalse($s->isReadable());
            static::assertTrue($s->isWritable());
            static::assertSame(4, $s->writeBytes('some'));
        });
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testReadingWriting(): void
    {
        File::readingWriting($this->file, static function (StreamInterface $s): void {
            static::assertTrue($s->isReadable());
            static::assertTrue($s->isWritable());
            static::assertSame(4, $s->writeBytes('some'));
            static::assertSame('some', $s->readAllBytes());
        });
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testTemporarily(): void
    {
        File::temporarily(static function (StreamInterface $s): void {
            static::assertTrue($s->isReadable());
            static::assertTrue($s->isWritable());
            static::assertSame(4, $s->writeBytes('some'));
            static::assertSame('some', $s->readAllBytes());
        });
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testCreate(): void
    {
        unlink($this->file);

        File::create($this->file);

        static::assertFileExists($this->file);
        static::assertSame(0, filesize($this->file));
    }

    /**
     * @expectedException \FileManagementTools\File\Exceptions\ElementAlreadyExistsException
     *
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testCreateOnExisting(): void
    {
        File::create($this->file);
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testCreateOnExistingIgnoreExisting(): void
    {
        File::create($this->file, true);

        static::assertFileExists($this->file);
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testCreateTemporary(): void
    {
        $path = '';

        File::createTemporary($path)->use(static function (StreamInterface $s): void {
            static::assertTrue($s->isReadable());
            static::assertTrue($s->isWritable());
            static::assertSame(4, $s->writeBytes('some'));
            static::assertSame('some', $s->readAllBytes());
        })
        ;

        static::assertFileExists($path);
        static::assertSame(4, filesize($path));

        unlink($path);
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testMoveFile(): void
    {
        $path = Path::join(sys_get_temp_dir(), 'test-temp.txt');

        File::move($this->file, $path);

        static::assertFileNotExists($this->file);

        File::move($path, $this->file);

        static::assertFileExists($this->file);
        static::assertSame(
            "file\r\nwith\nseveral\nlines\nand\naccents\n\xe9 \xe0 \xe7",
            file_get_contents($this->file)
        );
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testCopyFile(): void
    {
        $path = Path::join(sys_get_temp_dir(), 'test-temp.txt');

        try {
            File::copy($this->file, $path);

            static::assertSame("file\r\nwith\nseveral\nlines\nand\naccents\n\xe9 \xe0 \xe7", file_get_contents($path));
            static::assertSame(
                "file\r\nwith\nseveral\nlines\nand\naccents\n\xe9 \xe0 \xe7",
                file_get_contents($this->file)
            );
        } finally {
            if (is_file($path)) {
                unlink($path);
            }
        }
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testDelete(): void
    {
        File::delete($this->file);

        static::assertFileNotExists($this->file);
    }

    /**
     * @expectedException \FileManagementTools\File\Exceptions\FileNotFoundException
     *
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testDeleteNonExisting(): void
    {
        File::delete('some fake file');
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testDeleteNonExistingIgnore(): void
    {
        File::delete('some fake file', true);

        static::assertFileNotExists('some fake file');
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testGetSize(): void
    {
        static::assertSame(42, File::getSize($this->file));
    }

    /**
     * @expectedException \FileManagementTools\File\Exceptions\FileNotFoundException
     *
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testGetSizeNonExisting(): void
    {
        File::getSize('some fake file');
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testCreationDate(): void
    {
        $creationDate = File::getCreationDate($this->file)->getTimestamp();
        $now          = time();

        static::assertGreaterThanOrEqual($now - 3, $creationDate);
        static::assertLessThanOrEqual($now + 3, $creationDate);
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testModificationDate(): void
    {
        $date = \DateTime::createFromFormat('U', '1558113570');

        File::setModificationDate($this->file, $date);

        static::assertSame($date->getTimestamp(), File::getModificationDate($this->file, true)->getTimestamp());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testAccessDate(): void
    {
        $date = \DateTime::createFromFormat('U', '1558113570');

        File::setAccessDate($this->file, $date);

        static::assertSame($date->getTimestamp(), File::getAccessDate($this->file, true)->getTimestamp());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testGuessTypeFromContent(): void
    {
        $type = File::guessTypeFromContent(Path::join($this->files, 'test.zip'));

        static::assertNotNull($type);
        static::assertSame(FileType::ZIP, $type->getCode());
        static::assertSame(FileType::FAMILY_ARCHIVE, $type->getFamily());
    }

    public function testGuessTypeFromName(): void
    {
        $type = File::guessTypeFromName(Path::join($this->files, 'test.zip'));

        static::assertNotNull($type);
        static::assertSame(FileType::ZIP, $type->getCode());
        static::assertSame(FileType::FAMILY_ARCHIVE, $type->getFamily());
    }
}
