<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\Tests\File;

use FileManagementTools\File\Directory;
use FileManagementTools\File\Path;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \FileManagementTools\File\Directory
 */
final class DirectoryTest extends TestCase
{
    private $dir;

    protected function setUp()
    {
        $this->dir = tempnam(sys_get_temp_dir(), 'tests');

        unlink($this->dir);
        mkdir($this->dir);

        mkdir(Path::join($this->dir, 'folder1'));
        mkdir(Path::join($this->dir, 'folder1/folder2'));
        touch(Path::join($this->dir, 'folder1/file1'));
        mkdir(Path::join($this->dir, 'folder3'));
        touch(Path::join($this->dir, 'folder3/file2'));
        touch(Path::join($this->dir, 'folder3/file3'));
        mkdir(Path::join($this->dir, 'folder4'));
        touch(Path::join($this->dir, 'file4'));
        touch(Path::join($this->dir, 'file5'));
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    protected function tearDown()
    {
        Directory::delete($this->dir, true);
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testExists(): void
    {
        static::assertTrue(Directory::exists($this->dir));

        Directory::delete($this->dir);

        static::assertDirectoryNotExists($this->dir);
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testCreate(): void
    {
        $path = Path::join($this->dir, 'folder1', 'tmp');

        Directory::create($path);

        static::assertDirectoryExists($path);
    }

    /**
     * @expectedException \FileManagementTools\File\Exceptions\ElementAlreadyExistsException
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testCreateExisting(): void
    {
        Directory::create($this->dir);
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testCreateExistingIgnore(): void
    {
        Directory::create($this->dir, true);

        static::assertDirectoryExists($this->dir);
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testCreateAll(): void
    {
        $path = Path::join($this->dir, 'non', 'existing', 'directory');

        Directory::createAll($path);

        static::assertDirectoryExists($path);
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testMoveExisting(): void
    {
        $source      = Path::join($this->dir, 'folder1');
        $destination = Path::join($this->dir, 'folder3');

        Directory::move($source, $destination);

        static::assertDirectoryNotExists($source);
        static::assertFileExists(Path::join($destination, 'folder1/file1'));
        static::assertDirectoryExists(Path::join($destination, 'folder1/folder2'));
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testMoveNonExisting(): void
    {
        $source      = Path::join($this->dir, 'folder1');
        $destination = Path::join($this->dir, 'folder5');

        Directory::move($source, $destination);

        static::assertDirectoryNotExists($source);
        static::assertFileExists(Path::join($destination, 'file1'));
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testCopyExisting(): void
    {
        $source      = Path::join($this->dir, 'folder1');
        $destination = Path::join($this->dir, 'folder3');

        Directory::copy($source, $destination);

        static::assertDirectoryExists($source);
        static::assertFileExists(Path::join($destination, 'folder1/file1'));
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testCopyNonExisting(): void
    {
        $source      = Path::join($this->dir, 'folder1');
        $destination = Path::join($this->dir, 'folder5');

        Directory::copy($source, $destination);

        static::assertDirectoryExists($source);
        static::assertFileExists(Path::join($destination, 'file1'));
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testListEntriesRecursive(): void
    {
        $entries = Directory::listEntriesToArray($this->dir, true);

        asort($entries);

        static::assertSame(
            [
                'file4',
                'file5',
                'folder1',
                Path::join('folder1', 'file1'),
                Path::join('folder1', 'folder2'),
                'folder3',
                Path::join('folder3', 'file2'),
                Path::join('folder3', 'file3'),
                'folder4',
            ],
            $entries
        );
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testListEntries(): void
    {
        $entries = Directory::listEntriesToArray($this->dir);

        asort($entries);

        static::assertSame(['file4', 'file5', 'folder1', 'folder3', 'folder4'], $entries);
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testListEntriesRecursiveWithoutDirectories(): void
    {
        $entries = Directory::listEntriesToArray($this->dir, true, Directory::LIST_FILES);

        asort($entries);

        static::assertSame(
            [
                'file4',
                'file5',
                Path::join('folder1', 'file1'),
                Path::join('folder3', 'file2'),
                Path::join('folder3', 'file3'),
            ],
            $entries
        );
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testListEntriesRecursiveWithoutFiles(): void
    {
        $entries = Directory::listEntriesToArray($this->dir, true, Directory::LIST_DIRECTORIES);

        asort($entries);

        static::assertSame(['folder1', Path::join('folder1', 'folder2'), 'folder3', 'folder4'], $entries);
    }
}
