<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\Tests\TypeSniffer;

use FileManagementTools\File\File;
use FileManagementTools\File\Path;
use FileManagementTools\Stream\StreamInterface;
use FileManagementTools\TypeSniffer\DefaultFileTypeSniffer;
use FileManagementTools\TypeSniffer\FileType;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @covers \FileManagementTools\TypeSniffer\DefaultFileTypeSniffer
 */
final class DefaultFileTypeSnifferTest extends TestCase
{
    /**
     * @var string base path of test files
     */
    private $files;

    protected function setUp(): void
    {
        $this->files = Path::join(__DIR__, 'Files');
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingAiffFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.aiff'), static function (StreamInterface $s): void {
            $type = (new DefaultFileTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::AIFF, $type->getCode());
            static::assertSame(FileType::FAMILY_MEDIA, $type->getFamily());
        });
    }

    public function testSniffingAiffFileByFileName(): void
    {
        $type = (new DefaultFileTypeSniffer())->sniffName(Path::join($this->files, 'test.aiff'));

        static::assertNotNull($type);
        static::assertSame(FileType::AIFF, $type->getCode());
        static::assertSame(FileType::FAMILY_MEDIA, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingAviFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.avi'), static function (StreamInterface $s): void {
            $type = (new DefaultFileTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::AVI, $type->getCode());
            static::assertSame(FileType::FAMILY_MEDIA, $type->getFamily());
        });
    }

    public function testSniffingAviFileByFileName(): void
    {
        $type = (new DefaultFileTypeSniffer())->sniffName(Path::join($this->files, 'test.avi'));

        static::assertNotNull($type);
        static::assertSame(FileType::AVI, $type->getCode());
        static::assertSame(FileType::FAMILY_MEDIA, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingCabFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.cab'), static function (StreamInterface $s): void {
            $type = (new DefaultFileTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::CAB, $type->getCode());
            static::assertSame(FileType::FAMILY_ARCHIVE, $type->getFamily());
        });
    }

    public function testSniffingCabFileByFileName(): void
    {
        $type = (new DefaultFileTypeSniffer())->sniffName(Path::join($this->files, 'test.cab'));

        static::assertNotNull($type);
        static::assertSame(FileType::CAB, $type->getCode());
        static::assertSame(FileType::FAMILY_ARCHIVE, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingFlacFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.flac'), static function (StreamInterface $s): void {
            $type = (new DefaultFileTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::FLAC, $type->getCode());
            static::assertSame(FileType::FAMILY_MEDIA, $type->getFamily());
        });
    }

    public function testSniffingFlacFileByFileName(): void
    {
        $type = (new DefaultFileTypeSniffer())->sniffName(Path::join($this->files, 'test.flac'));

        static::assertNotNull($type);
        static::assertSame(FileType::FLAC, $type->getCode());
        static::assertSame(FileType::FAMILY_MEDIA, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingIsoFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.iso'), static function (StreamInterface $s): void {
            $type = (new DefaultFileTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::ISO, $type->getCode());
            static::assertSame(FileType::FAMILY_ARCHIVE, $type->getFamily());
        });
    }

    public function testSniffingIsoFileByFileName(): void
    {
        $type = (new DefaultFileTypeSniffer())->sniffName(Path::join($this->files, 'test.iso'));

        static::assertNotNull($type);
        static::assertSame(FileType::ISO, $type->getCode());
        static::assertSame(FileType::FAMILY_ARCHIVE, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingMidiFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.mid'), static function (StreamInterface $s): void {
            $type = (new DefaultFileTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::MIDI, $type->getCode());
            static::assertSame(FileType::FAMILY_MEDIA, $type->getFamily());
        });
    }

    public function testSniffingMidiFileByFileName(): void
    {
        $type = (new DefaultFileTypeSniffer())->sniffName(Path::join($this->files, 'test.mid'));

        static::assertNotNull($type);
        static::assertSame(FileType::MIDI, $type->getCode());
        static::assertSame(FileType::FAMILY_MEDIA, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingMp3FileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.mp3'), static function (StreamInterface $s): void {
            $type = (new DefaultFileTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::MP3, $type->getCode());
            static::assertSame(FileType::FAMILY_MEDIA, $type->getFamily());
        });
    }

    public function testSniffingMp3FileByFileName(): void
    {
        $type = (new DefaultFileTypeSniffer())->sniffName(Path::join($this->files, 'test.mp3'));

        static::assertNotNull($type);
        static::assertSame(FileType::MP3, $type->getCode());
        static::assertSame(FileType::FAMILY_MEDIA, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingMpegFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.mpg'), static function (StreamInterface $s): void {
            $type = (new DefaultFileTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::MPEG, $type->getCode());
            static::assertSame(FileType::FAMILY_MEDIA, $type->getFamily());
        });
    }

    public function testSniffingMpegFileByFileName(): void
    {
        $type = (new DefaultFileTypeSniffer())->sniffName(Path::join($this->files, 'test.mpg'));

        static::assertNotNull($type);
        static::assertSame(FileType::MPEG, $type->getCode());
        static::assertSame(FileType::FAMILY_MEDIA, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingOggFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.ogg'), static function (StreamInterface $s): void {
            $type = (new DefaultFileTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::OGG, $type->getCode());
            static::assertSame(FileType::FAMILY_MEDIA, $type->getFamily());
        });
    }

    public function testSniffingOggFileByFileName(): void
    {
        $type = (new DefaultFileTypeSniffer())->sniffName(Path::join($this->files, 'test.ogg'));

        static::assertNotNull($type);
        static::assertSame(FileType::OGG, $type->getCode());
        static::assertSame(FileType::FAMILY_MEDIA, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingRarFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.rar'), static function (StreamInterface $s): void {
            $type = (new DefaultFileTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::RAR, $type->getCode());
            static::assertSame(FileType::FAMILY_ARCHIVE, $type->getFamily());
        });
    }

    public function testSniffingRarFileByFileName(): void
    {
        $type = (new DefaultFileTypeSniffer())->sniffName(Path::join($this->files, 'test.rar'));

        static::assertNotNull($type);
        static::assertSame(FileType::RAR, $type->getCode());
        static::assertSame(FileType::FAMILY_ARCHIVE, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingElfFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.so'), static function (StreamInterface $s): void {
            $type = (new DefaultFileTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::ELF, $type->getCode());
            static::assertSame(FileType::FAMILY_BINARIES, $type->getFamily());
        });
    }

    public function testSniffingElfFileByFileName(): void
    {
        $type = (new DefaultFileTypeSniffer())->sniffName(Path::join($this->files, 'test.so'));

        static::assertNotNull($type);
        static::assertSame(FileType::ELF, $type->getCode());
        static::assertSame(FileType::FAMILY_BINARIES, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingTarFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.tar'), static function (StreamInterface $s): void {
            $type = (new DefaultFileTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::TAR, $type->getCode());
            static::assertSame(FileType::FAMILY_ARCHIVE, $type->getFamily());
        });
    }

    public function testSniffingTarFileByFileName(): void
    {
        $type = (new DefaultFileTypeSniffer())->sniffName(Path::join($this->files, 'test.tar'));

        static::assertNotNull($type);
        static::assertSame(FileType::TAR, $type->getCode());
        static::assertSame(FileType::FAMILY_ARCHIVE, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingGzipFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.tar.gz'), static function (StreamInterface $s): void {
            $type = (new DefaultFileTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::GZ, $type->getCode());
            static::assertSame(FileType::FAMILY_ARCHIVE, $type->getFamily());
        });
    }

    public function testSniffingGzipFileByFileName(): void
    {
        $type = (new DefaultFileTypeSniffer())->sniffName(Path::join($this->files, 'test.tar.gz'));

        static::assertNotNull($type);
        static::assertSame(FileType::GZ, $type->getCode());
        static::assertSame(FileType::FAMILY_ARCHIVE, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingBzip2FileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.bz2'), static function (StreamInterface $s): void {
            $type = (new DefaultFileTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::BZ2, $type->getCode());
            static::assertSame(FileType::FAMILY_ARCHIVE, $type->getFamily());
        });
    }

    public function testSniffingBzip2FileByFileName(): void
    {
        $type = (new DefaultFileTypeSniffer())->sniffName(Path::join($this->files, 'test.bz2'));

        static::assertNotNull($type);
        static::assertSame(FileType::BZ2, $type->getCode());
        static::assertSame(FileType::FAMILY_ARCHIVE, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingLzmaFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.xz'), static function (StreamInterface $s): void {
            $type = (new DefaultFileTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::LZMA, $type->getCode());
            static::assertSame(FileType::FAMILY_ARCHIVE, $type->getFamily());
        });
    }

    public function testSniffingLzmaFileByFileName(): void
    {
        $type = (new DefaultFileTypeSniffer())->sniffName(Path::join($this->files, 'test.xz'));

        static::assertNotNull($type);
        static::assertSame(FileType::LZMA, $type->getCode());
        static::assertSame(FileType::FAMILY_ARCHIVE, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingWaveFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.wav'), static function (StreamInterface $s): void {
            $type = (new DefaultFileTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::WAV, $type->getCode());
            static::assertSame(FileType::FAMILY_MEDIA, $type->getFamily());
        });
    }

    public function testSniffingWaveFileByFileName(): void
    {
        $type = (new DefaultFileTypeSniffer())->sniffName(Path::join($this->files, 'test.wav'));

        static::assertNotNull($type);
        static::assertSame(FileType::WAV, $type->getCode());
        static::assertSame(FileType::FAMILY_MEDIA, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingWmaFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.wma'), static function (StreamInterface $s): void {
            $type = (new DefaultFileTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::WMA, $type->getCode());
            static::assertSame(FileType::FAMILY_MEDIA, $type->getFamily());
        });
    }

    public function testSniffingWmaFileByFileName(): void
    {
        $type = (new DefaultFileTypeSniffer())->sniffName(Path::join($this->files, 'test.wma'));

        static::assertNotNull($type);
        static::assertSame(FileType::WMA, $type->getCode());
        static::assertSame(FileType::FAMILY_MEDIA, $type->getFamily());
    }

    /**
     * @throws \FileManagementTools\File\Exceptions\IOException
     */
    public function testSniffingZipFileByContent(): void
    {
        File::reading(Path::join($this->files, 'test.zip'), static function (StreamInterface $s): void {
            $type = (new DefaultFileTypeSniffer())->sniffContent($s);

            static::assertNotNull($type);
            static::assertSame(FileType::ZIP, $type->getCode());
            static::assertSame(FileType::FAMILY_ARCHIVE, $type->getFamily());
        });
    }

    public function testSniffingZipFileByFileName(): void
    {
        $type = (new DefaultFileTypeSniffer())->sniffName(Path::join($this->files, 'test.zip'));

        static::assertNotNull($type);
        static::assertSame(FileType::ZIP, $type->getCode());
        static::assertSame(FileType::FAMILY_ARCHIVE, $type->getFamily());
    }
}
