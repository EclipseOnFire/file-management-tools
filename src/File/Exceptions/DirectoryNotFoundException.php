<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\File\Exceptions;

/**
 * Thrown when a directory does not exist.
 */
class DirectoryNotFoundException extends IOException
{
    public function __construct(string $dir)
    {
        parent::__construct("Directory '{$dir}' does not exists!");
    }
}
