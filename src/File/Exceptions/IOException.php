<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\File\Exceptions;

use FileManagementTools\Exceptions\IllegalStateException;

/**
 * Generic exception for file-related errors.
 */
class IOException extends \Exception
{
    /**
     * Creates a new generic IOException from the last PHP error.
     *
     * @return IOException the created exception
     */
    public static function fromLastError(): self
    {
        $lastError = error_get_last();

        if ($lastError === null) {
            throw new IllegalStateException('No last error was found!');
        }

        return new self($lastError['message'], $lastError['type']);
    }
}
