<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\Stream;

/**
 * A stream that wraps a simple binary string.
 */
class MemoryStream extends ResourceStream
{
    /**
     * Creates a new memory stream.
     *
     * @param bool   $readable `true` if the stream should be readable, `false` otherwise
     * @param bool   $writable `true` if the stream should be readable, `false` otherwise
     * @param string $initData a binary string containing the bytes that the stream should contain
     */
    public function __construct(bool $readable, bool $writable, ?string $initData)
    {
        if (!$readable && !$writable) {
            throw new \InvalidArgumentException('The stream must be at least readable or writable!');
        }

        $handle = @fopen('php://memory', ($readable ? 'r' : null) . ($writable ? 'w' : null) . 'b');

        if ($handle === false) {
            throw new \RuntimeException('Unexpected error while opening a handle to "php://memory"!');
        }

        if ($initData !== null && (@fwrite($handle, $initData)) === false) {
            throw new \RuntimeException('Unexpected error while writing to the "php://memory" handle!');
        }

        parent::__construct($handle);
    }
}
