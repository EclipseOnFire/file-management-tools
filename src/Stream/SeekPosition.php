<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\Stream;

/**
 * Enum of seek positions.
 */
final class SeekPosition
{
    /**
     * Beginning of the stream.
     */
    public const START = 0;

    /**
     * Relative position from the cursor.
     */
    public const CURSOR = 1;

    /**
     * End of the file position.
     */
    public const END = 2;

    private function __construct()
    {
    }
}
