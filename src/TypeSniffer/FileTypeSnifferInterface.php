<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\TypeSniffer;

use FileManagementTools\File\Exceptions\IOException;
use FileManagementTools\Stream\StreamInterface;

/**
 * Describes a class able to detect file types.
 */
interface FileTypeSnifferInterface
{
    /**
     * Tries to guess a file's type from its content.
     *
     * @param StreamInterface $stream the file's stream
     *
     * @throws IOException
     *
     * @return null|FileType the file's type or `null` if not recognized
     */
    public function sniffContent(StreamInterface $stream): ?FileType;

    /**
     * Tries to guess a file's type from its extension.
     *
     * @param string $fileName the file's name
     *
     * @return null|FileType the file's type or `null` if not recognized
     */
    public function sniffName(string $fileName): ?FileType;
}
