<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\TypeSniffer;

use FileManagementTools\File\Exceptions\IOException;
use FileManagementTools\Stream\StreamInterface;

/**
 * Class that holds all existing file sniffers.
 */
final class FileTypeSnifferRegistry
{
    /**
     * @var null|FileTypeSnifferRegistry the singleton instance
     */
    private static $instance;

    /**
     * @var FileTypeSnifferInterface[] the list of all file sniffers
     */
    private $fileSniffers = [];

    private function __construct()
    {
        $this->fileSniffers[] = new MicrosoftOfficeTypeSniffer();
        $this->fileSniffers[] = new DefaultFileTypeSniffer();
    }

    /**
     * Adds the provided file sniffer.
     *
     * @param FileTypeSnifferInterface $fileSniffer the sniffer to add
     */
    public function register(FileTypeSnifferInterface $fileSniffer): void
    {
        $this->fileSniffers[] = $fileSniffer;
    }

    /**
     * Removes all existing file sniffers.
     */
    public function clearFileSniffers(): void
    {
        $this->fileSniffers[] = [];
    }

    /**
     * Tries to guess the file type from the provided stream.
     *
     * @param StreamInterface $stream the stream
     *
     * @throws IOException
     *
     * @return FileType the guessed file type
     */
    public function guessFromContent(StreamInterface $stream): FileType
    {
        if (!$stream->isReadable()) {
            throw new \InvalidArgumentException('The provided stream must be readable!');
        }

        foreach ($this->fileSniffers as $sniffer) {
            $result = $sniffer->sniffContent($stream->seek(0));

            if ($result !== null) {
                return $result;
            }
        }

        return new FileType('Unknown', [], ['application/octet-stream'], 0);
    }

    /**
     * Tries to guess z file's type from its name.
     *
     * @param string $name the file's name
     *
     * @return FileType the guessed file type
     */
    public function guessFromName(string $name): FileType
    {
        foreach ($this->fileSniffers as $sniffer) {
            $result = $sniffer->sniffName($name);

            if ($result !== null) {
                return $result;
            }
        }

        return new FileType('Unknown', [], ['application/octet-stream'], 0);
    }

    /**
     * @return FileTypeSnifferRegistry the registry instance
     */
    public static function getInstance(): self
    {
        return self::$instance ?? (self::$instance = new self());
    }
}
