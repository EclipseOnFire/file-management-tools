<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\TypeSniffer;

use FileManagementTools\File\Path;
use FileManagementTools\Stream\StreamInterface;

/**
 * File sniffer that parses file types from an array.
 */
class DefaultFileTypeSniffer implements FileTypeSnifferInterface
{
    /**
     * @var array
     */
    private $types;

    public function __construct()
    {
        $this->types = [
            [
                'code'       => FileType::DLL,
                'name'       => 'Dynamic Linking Library',
                'extensions' => ['dll'],
                'mimes'      => ['application/octet-stream'],
                'detector'   => static function (StreamInterface $s): bool {
                    if ($s->readBytes(2) !== 'MZ') {
                        return false;
                    }

                    // Retrieve the PE location.
                    $pos = $s->seek(60)->readUnsignedShort();

                    if ($s->seek($pos)->readBytes(4) !== "PE\0\0") {
                        return false;
                    }

                    return (bool) ($s->seek($pos + 22)->readUnsignedShort() & 0x2000);
                },
            ],
            [
                'code'       => FileType::EXE,
                'name'       => 'Windows executable file',
                'extensions' => ['exe'],
                'mimes'      => ['application/octet-stream'],
                'detector'   => static function (StreamInterface $s): bool {
                    if ($s->readBytes(2) !== 'MZ') {
                        return false;
                    }

                    // Retrieve the PE location.
                    $pos = $s->seek(60)->readUnsignedShort();

                    if ($s->seek($pos)->readBytes(4) !== "PE\0\0") {
                        return false;
                    }

                    return (bool) ($s->seek($pos + 22)->readUnsignedShort() & 0x012);
                },
            ],
            [
                'code'       => FileType::ELF,
                'name'       => 'Linux binary file',
                'extensions' => ['so'],
                'mimes'      => ['application/octet-stream'],
                'detector'   => static function (StreamInterface $s): bool {
                    return $s->readBytes(4) === "\x7fELF";
                },
            ],
            [
                'code'       => FileType::CSV,
                'name'       => 'Comma-separated values',
                'extensions' => ['csv'],
                'mimes'      => ['text/csv'],
            ],
            [
                'code'       => FileType::TSV,
                'name'       => 'Tab-separated values',
                'extensions' => ['tsv'],
                'mimes'      => ['text/csv'],
            ],
            [
                'code'       => FileType::WAV,
                'name'       => 'Waveform audio file format',
                'extensions' => ['wav'],
                'mimes'      => ['audio/wav'],
                'detector'   => static function (StreamInterface $s): bool {
                    $firstBytes = $s->readBytes(4);

                    return $firstBytes === 'WAVE' || ($firstBytes === 'RIFF' && $s->seek(8)->readBytes(4) === 'WAVE');
                },
            ],
            [
                'code'       => FileType::AIFF,
                'name'       => 'Audio Interchange File Format',
                'extensions' => ['aiff', 'aif', 'aifc', 'snd', 'iff'],
                'mimes'      => ['audio/aiff', 'audio/x-aiff'],
                'detector'   => static function (StreamInterface $s): bool {
                    return $s->readBytes(4) === 'FORM' && $s->seek(8)->readBytes(4) === 'AIFF';
                },
            ],
            [
                'code'       => FileType::FLAC,
                'name'       => 'Free Lossless Audio Codec',
                'extensions' => ['flac'],
                'mimes'      => ['audio/flac'],
                'detector'   => static function (StreamInterface $s): bool {
                    return $s->readBytes(4) === 'fLaC';
                },
            ],
            [
                'code'       => FileType::MIDI,
                'name'       => 'MIDI',
                'extensions' => ['mid', 'midi'],
                'mimes'      => ['audio/midi'],
                'detector'   => static function (StreamInterface $s): bool {
                    return $s->readBytes(4) === 'MThd';
                },
            ],
            [
                'code'       => FileType::WMA,
                'name'       => 'Advanced Systems Format',
                'extensions' => ['asf', 'wma', 'wmv'],
                'mimes'      => ['audio/wma'],
                'detector'   => static function (StreamInterface $s): bool {
                    return $s->readBytes(16) === "\x30\x26\xb2\x75\x8e\x66\xcf\x11\xa6\xd9\x00\xaa\x00\x62\xce\x6c";
                },
            ],
            [
                'code'       => FileType::MP3,
                'name'       => 'MP3',
                'extensions' => ['mp3'],
                'mimes'      => ['audio/mpeg', 'audio/MPA', 'audio/mpa-robust'],
                'detector'   => static function (StreamInterface $s): bool {
                    return $s->readBytes(2) === "\xff\xfb" || $s->seek(0)->readBytes(3) === 'ID3';
                },
            ],
            [
                'code'       => FileType::OGG,
                'name'       => 'OGG',
                'extensions' => ['ogg', 'oga', 'ogv'],
                'mimes'      => ['video/ogg', 'audio/ogg', 'application/ogg'],
                'detector'   => static function (StreamInterface $s): bool {
                    return $s->readBytes(4) === 'OggS';
                },
            ],
            [
                'code'       => FileType::MPEG,
                'name'       => 'MPEG',
                'extensions' => ['m2p', 'mpeg', 'mpg', 'mpe'],
                'mimes'      => ['video/mpeg'],
                'detector'   => static function (StreamInterface $s): bool {
                    $bytes = $s->readBytes(4);

                    return $bytes === "\x00\x00\x01\xba" || $bytes === "\00\x00\x01\xb3" || $bytes[0] === 'G';
                },
            ],
            [
                'code'       => FileType::AVI,
                'name'       => 'Audio Video Interleave',
                'extensions' => ['avi'],
                'mimes'      => ['video/avi'],
                'detector'   => static function (StreamInterface $s): bool {
                    return $s->readBytes(4) === 'RIFF' && $s->seek(8)->readBytes(3) === 'AVI';
                },
            ],
            [
                'code'       => FileType::UA,
                'name'       => 'Unix Archiver',
                'extensions' => ['a', 'ar'],
                'mimes'      => ['application/x-archive'],
                'detector'   => static function (StreamInterface $s): bool {
                    return $s->readBytes(8) === "!<arch>\n";
                },
            ],
            [
                'code'       => FileType::TAR,
                'name'       => 'Tape archive',
                'extensions' => ['tar'],
                'mimes'      => ['application/x-tar'],
                'detector'   => static function (StreamInterface $s): bool {
                    return $s->seek(257)->readBytes(5) === 'ustar';
                },
            ],
            [
                'code'       => FileType::ISO,
                'name'       => 'ISO-9660 image',
                'extensions' => ['iso'],
                'mimes'      => ['application/x-iso9660-image'],
                'detector'   => static function (StreamInterface $s): bool {
                    return $s->seek(0x8001)->readBytes(5) === 'CD001' ||
                        $s->seek(0x8801)->readBytes(5) === 'CD001' ||
                        $s->seek(0x9001)->readBytes(5) === 'CD001';
                },
            ],
            [
                'code'       => FileType::BZ2,
                'name'       => 'bzip2',
                'extensions' => ['bz2'],
                'mimes'      => ['application/x-bzip2'],
                'detector'   => static function (StreamInterface $s): bool {
                    return $s->readBytes(3) === 'BZh';
                },
            ],
            [
                'code'       => FileType::GZ,
                'name'       => 'gzip',
                'extensions' => ['gz'],
                'mimes'      => ['application/gzip'],
                'detector'   => static function (StreamInterface $s): bool {
                    return $s->readBytes(2) === "\x1f\x8b";
                },
            ],
            [
                'code'       => FileType::LZ,
                'name'       => 'lzip',
                'extensions' => ['lz'],
                'mimes'      => ['application/x-lzip'],
                'detector'   => static function (StreamInterface $s): bool {
                    return $s->readBytes(4) === 'LZIP';
                },
            ],
            [
                'code'       => FileType::LZMA,
                'name'       => 'Lempel–Ziv–Markov Algorithm archive',
                'extensions' => ['lzma', 'xz'],
                'mimes'      => ['application/x-lzma'],
                'detector'   => static function (StreamInterface $s): bool {
                    return $s->seek(1)->readBytes(4) === '7zXZ';
                },
            ],
            [
                'code'       => FileType::CAB,
                'name'       => 'Cabinet',
                'extensions' => ['cab'],
                'mimes'      => ['application/vnd.ms-cab-compressed'],
                'detector'   => static function (StreamInterface $s): bool {
                    return $s->readBytes(4) === 'MSCF';
                },
            ],
            [
                'code'       => FileType::RAR,
                'name'       => 'RAR',
                'extensions' => ['rar'],
                'mimes'      => ['application/x-rar-compressed'],
                'detector'   => static function (StreamInterface $s): bool {
                    return $s->readBytes(4) === 'Rar!';
                },
            ],
            // Important note: since many formats are using zip compression, it is better to have this format at the
            // end of the list.
            [
                'code'       => FileType::ZIP,
                'name'       => 'ZIP',
                'extensions' => ['zip'],
                'mimes'      => ['application/zip'],
                'detector'   => static function (StreamInterface $s): bool {
                    return $s->readBytes(2) === 'PK' &&
                        \in_array($s->readBytes(2), ["\x03\x04", "\x05\x06", "\x07\x08"], true);
                },
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function sniffContent(StreamInterface $stream): ?FileType
    {
        foreach ($this->types as $type) {
            if (isset($type['detector']) && $type['detector']($stream->seek(0))) {
                return $this->createFileTypeFromArray($type);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function sniffName(string $fileName): ?FileType
    {
        $fileExtension = Path::getExtension($fileName);

        if ($fileExtension === null) {
            return null;
        }

        foreach ($this->types as $type) {
            if (\in_array($fileExtension, $type['extensions'], true)) {
                return $this->createFileTypeFromArray($type);
            }
        }

        return null;
    }

    private function createFileTypeFromArray(array $type): FileType
    {
        return new FileType($type['name'], $type['extensions'], $type['mimes'], $type['code']);
    }
}
