<?php

declare(strict_types=1);

/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace FileManagementTools\TypeSniffer;

use FileManagementTools\Encoding\Endianness;
use FileManagementTools\File\Exceptions\EndOfStreamException;
use FileManagementTools\File\File;
use FileManagementTools\File\Path;
use FileManagementTools\Stream\StreamInterface;
use ZipArchive;

/**
 * File sniffer that recognizes file types of usual office documents.
 */
class MicrosoftOfficeTypeSniffer implements FileTypeSnifferInterface
{
    /**
     * @var FileType the DOC file format
     */
    private $docFormat;

    /**
     * @var FileType the XLS file format
     */
    private $xlsFormat;

    /**
     * @var FileType the PPT file format
     */
    private $pptFormat;

    /**
     * @var FileType the DOCX file format
     */
    private $docxFormat;

    /**
     * @var FileType the XLSX file format
     */
    private $xlsxFormat;

    /**
     * @var FileType the PPTX file format
     */
    private $pptxFormat;

    public function __construct()
    {
        $this->docFormat = new FileType('Office Word document', ['doc'], ['application/vnd.ms-word'], FileType::DOC);

        $this->xlsFormat = new FileType(
            'Office Excel spreadsheet',
            ['xls'],
            ['application/vnd.ms-excel'],
            FileType::XLS
        );

        $this->pptFormat = new FileType(
            'Office Powerpoint presentation',
            ['ppt'],
            ['application/vnd.ms-powerpoint'],
            FileType::PPT
        );

        $this->docxFormat = new FileType(
            'Office Word document',
            ['docx'],
            ['application/vnd.openxmlformats-officedocument.wordprocessingml.document'],
            FileType::DOCX
        );

        $this->xlsxFormat = new FileType(
            'Office Excel spreadsheet',
            ['xlsx'],
            ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
            FileType::XLSX
        );

        $this->pptxFormat = new FileType(
            'Office Powerpoint presentation',
            ['pptx'],
            ['application/vnd.openxmlformats-officedocument.presentationml.presentation'],
            FileType::PPTX
        );
    }

    /**
     * {@inheritdoc}
     */
    public function sniffContent(StreamInterface $stream): ?FileType
    {
        try {
            if ($stream->readBytes(2) === 'PK') {
                // Seems to be a ZIP file, so we're dealing with modern OpenXML formats.

                foreach (self::listZipEntries($stream->seek(0)) as $entry) {
                    if (strpos($entry, 'word') !== false) {
                        return $this->docxFormat;
                    }

                    if (strpos($entry, 'ppt') !== false) {
                        return $this->pptxFormat;
                    }

                    if (strpos($entry, 'xl') !== false) {
                        return $this->xlsxFormat;
                    }
                }
            } elseif ($stream->seek(0)->readBytes(8) === "\xd0\xcf\x11\xe0\xa1\xb1\x1a\xe1") {
                // Seems to be the good old proprietary format.

                // Read the file's metadata.
                $sectorSize = 1 << $stream->seek(30)->readUnsignedShort(Endianness::LITTLE);
                $rootDirPos = $stream->seek(48)->readUnsignedInt(Endianness::LITTLE);

                // Seek the root sector position.
                $stream->seek($sectorSize * (1 + $rootDirPos) + 80);

                switch ($stream->readBytes(16)) {
                    case "\x10\x08\x02\x00\x00\x00\x00\x00\xc0\x00\x00\x00\x00\x00\x00\x46":
                    case "\x20\x08\x02\x00\x00\x00\x00\x00\xc0\x00\x00\x00\x00\x00\x00\x46":
                        return $this->xlsFormat;
                    case "\x06\x09\x02\x00\x00\x00\x00\x00\xc0\x00\x00\x00\x00\x00\x00\x46":
                        return $this->docFormat;
                    case "\x10\x8d\x81\x64\x9b\x4f\xcf\x11\x86\xea\x00\xaa\x00\xb9\x29\xe8":
                        return $this->pptFormat;
                }
            }
        } catch (EndOfStreamException $ignored) {
            // Ignore and return null.
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function sniffName(string $fileName): ?FileType
    {
        switch (Path::getExtension($fileName)) {
            case 'doc':
                return $this->docFormat;
            case 'xls':
                return $this->xlsFormat;
            case 'ppt':
                return $this->pptFormat;
            case 'docx':
                return $this->docxFormat;
            case 'xlsx':
                return $this->xlsxFormat;
            case 'pptx':
                return $this->pptxFormat;
            default:
                return null;
        }
    }

    /**
     * @param StreamInterface $zipStream
     *
     * @throws \FileManagementTools\File\Exceptions\IOException
     *
     * @return null|array
     *
     * @deprecated Implement own zip functions in order to be extension-independent!
     */
    private static function listZipEntries(StreamInterface $zipStream): ?array
    {
        $path = '';

        try {
            return File::createTemporary($path)
                ->use(static function (StreamInterface $tmp) use ($zipStream, $path): ?array {
                    $zipStream->copyTo($tmp);
                    $tmp->close();

                    $zip = new ZipArchive();

                    if ($zip->open($path, ZipArchive::CHECKCONS) !== true) {
                        return null;
                    }

                    $result = [];

                    try {
                        for ($i = 0; $i < $zip->numFiles; ++$i) {
                            $result[] = $zip->getNameIndex($i);
                        }
                    } finally {
                        $zip->close();
                    }

                    return $result;
                })
                ;
        } finally {
            File::delete($path, true);
        }
    }
}
