FROM php:7.1-cli
COPY --from=composer /usr/bin/composer /usr/bin/composer
RUN apt-get update \
    && apt-get install -y unzip libzip-dev \
    && docker-php-ext-install zip \
    && useradd build \
    && mkdir /opt/buildagent \
    && chown -R build:build /opt/buildagent
USER build